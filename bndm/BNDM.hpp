/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file BNDM.hpp
 *  @class BNDM
 *  @brief Implementation of Backward Nondeterministic Dawg Matching algorithm
 *
 * This class implements the Backward Nondeterministic Dawg Matching algorithm.
 *
 * More information can be found in:
 *
 * NAVARRO G., RAFFINOT M., 1998. A Bit-Parallel Approach to Suffix Automata:
 * Fast Extended String Matching, In Proceedings of the 9th Annual Symposium on
 * Combinatorial Pattern Matching, Lecture Notes in Computer Science 1448,
 * Springer-Verlag, Berlin, 14-31.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef BNDM_HPP
#define BNDM_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class BNDM : public Algorithm {
public:
  BNDM(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::unordered_map<wchar_t, boost::dynamic_bitset<>> table;
};

BNDM::BNDM(const std::wstring pattern) : pattern(pattern), table() {
  this->preprocess();
}

auto BNDM::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  boost::dynamic_bitset<> empty_bitset(m);

  for (auto i = 0U; i < m; ++i) {
    auto index = (m - 1) - i;
    auto char_finder = table.find(this->pattern[i]);

    if (char_finder == table.end()) {
      auto new_bitset = empty_bitset;
      new_bitset[index] = true;

      table.emplace(this->pattern[i], new_bitset);

    } else {
      char_finder->second[index] = true;
    }
  }
}

auto BNDM::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  int j = 0;
  int last = 0;
  int pos = 0;

  boost::dynamic_bitset<> D{m, 0};

  boost::dynamic_bitset<> D_found{m, 0};

  D_found[m - 1] = 1;

  if (n >= m) {
    while (pos <= (int)(n - m)) {
      j = m - 1;
      last = m;

      D.set();

      while (D != boost::dynamic_bitset<>{m, 0}) {
        auto char_finder = this->table.find(text[pos + j]);
        auto bit_vector = char_finder != this->table.end()
                              ? char_finder->second
                              : boost::dynamic_bitset<>{m};

        D = D & bit_vector;
        --j;

        if (D[m - 1]) {
          if (j >= 0) {
            last = j + 1; // + 1;
          } else {
            positions.emplace_back(pos);
          }
        }
        D <<= 1;
      }
      pos = pos + last;
    }
  }
  return positions;
}

#endif
