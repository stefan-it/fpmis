/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file KMPNaive.hpp
 *  @class KMPNaive
 *  @brief Implementation of naive Knuth-Morris-Pratt algorithm
 *
 * This class implements the naive Knuth-Morris-Pratt algorithm
 *
 * More information can be found in:
 *
 * KNUTH D.E., MORRIS (Jr) J.H., PRATT V.R., 1977,
 * Fast pattern matching in strings, SIAM Journal on Computing 6(1):323-350.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef KMP_NAIVE_HPP
#define KMP_NAIVE_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"


class KMPNaive : public Algorithm {
public:
  KMPNaive(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::vector<signed int> table;
};

KMPNaive::KMPNaive(const std::wstring pattern) : pattern(pattern), table() {
  this->preprocess();
}

auto KMPNaive::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  auto i = 0U;
  signed int j = -1;

  this->table.resize(m+1, -1);

  for (; i < m; ++i, ++j) {
    while (j >= 0 and this->pattern[j] != this->pattern[i]) {
      j = this->table[j];
    }
    this->table[i+1] = j + 1;
  }
}

auto KMPNaive::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  auto i = 0U;
  signed int j = 0;

  if (n >= m) {
    for (; i < n;) {
      while (j >= 0 and text[i] != this->pattern[j]) {
        j = this->table[j];
      }

      ++i;
      ++j;

      if (j == (int)m) {
        positions.emplace_back(i - m);

        j = this->table[j];
      }
    }
  }
  return positions;
}

#endif
