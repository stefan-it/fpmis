/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file BM.hpp
 *  @class BM
 *  @brief Implementation of Boyer-Moore algorithm
 *
 * This class implements the Boyer-Moore algorithm and is inspired by the C
 * implementation from Thierry Lecroq:
 *
 * http://www-igm.univ-mlv.fr/~lecroq/string/node14.html
 *
 * More information can be found in:
 *
 * BOYER R.S., MOORE J.S., 1977, A fast string searching algorithm.
 * Communications of the ACM. 20:762-772.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef BM_HPP
#define BM_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class BM : public Algorithm {
public:
  BM(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::unordered_map<wchar_t, signed int> bad_character_table;
  std::vector<signed int> suffixes;
  std::vector<signed int> good_suffix_table;
};

BM::BM(const std::wstring pattern)
    : pattern(pattern), bad_character_table(), suffixes(), good_suffix_table() {
  this->preprocess();
}

auto BM::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  auto bad_character_shift = [&]() {
    for (auto i = 0U; i < (m - 1); ++i) {
      this->bad_character_table[this->pattern[i]] = m - i - 1;
    }
  };

  auto suffixes = [&]() {
    int f, g, i;

    this->suffixes.resize(m + 1, m);

    this->suffixes[m - 1] = m;

    g = m - 1;
    f = 0;

    for (i = m - 2; i >= 0; --i) {
      if (i > g && this->suffixes[i + m - 1 - f] < i - g) {
        this->suffixes[i] = this->suffixes[i + m - 1 - f];
      } else {
        if (i < g) {
          g = i;
        }

        f = i;

        while (g >= 0 && this->pattern[g] == this->pattern[g + m - 1 - f]) {
          --g;
        }
        this->suffixes[i] = f - g;
      }
    }
  };

  auto good_suffix_shift = [&]() {
    suffixes();

    this->good_suffix_table.resize(m + 1, m);

    int i, j;

    j = 0;

    for (i = m - 1; i >= -1; --i) {
      if (i == -1 || this->suffixes[i] == i + 1) {
        for (; j < (int)m - 1 - i; ++j) {
          if (this->good_suffix_table[j] == (int)m) {
            this->good_suffix_table[j] = m - 1 - i;
          }
        }
      }
    }

    for (i = 0; i <= (int)m - 2; ++i) {
      this->good_suffix_table[m - 1 - this->suffixes[i]] = (int)m - 1 - i;
    }
  };

  bad_character_shift();
  good_suffix_shift();
}

auto BM::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  int i, j;

  j = 0;

  if (n >= m) {
    while (j <= (int)(n - m)) {
      for (i = m - 1; i >= 0 && this->pattern[i] == text[i + j]; --i)
        ;

      if (i < 0) {
        positions.emplace_back(j);
        j += this->good_suffix_table[0];
      } else {
        auto char_finder = this->bad_character_table.find(text[i + j]);
        j += std::max(this->good_suffix_table[i],
                      (char_finder != this->bad_character_table.end()
                           ? char_finder->second
                           : (int)m) -
                          (int)m + 1 + i);
      }
    }
  }
  return positions;
}

#endif
