/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file QS.hpp
 *  @class QS
 *  @brief Implementation of Quick Search algorithm
 *
 * This class implements the Quick Search algorithm and is inspired by the C
 * implementation from Thierry Lecroq:
 *
 * http://www-igm.univ-mlv.fr/~lecroq/string/node19.html
 *
 * More information can be found in:
 *
 * SUNDAY D.M., 1990, A very fast substring search algorithm,
 * Communications of the ACM . 33(8):132-142.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef QS_HPP
#define QS_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class QS : public Algorithm {
public:
  QS(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::unordered_map<wchar_t, size_t> table;
};

QS::QS(const std::wstring pattern) : pattern(pattern), table() {
  this->preprocess();
}

auto QS::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  for (auto i = 0U; i < m; ++i) {
    this->table[this->pattern[i]] = m - i;
  }
}

auto QS::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  auto j = 0U;

  if (n >= m) {
    while (j <= n - m) {
      if (wmemcmp(this->pattern.c_str(), text.c_str() + j, m) == 0) {
        positions.emplace_back(j);
      }

      auto char_finder = this->table.find(text[j + m]);
      j += char_finder != this->table.end() ? char_finder->second : m + 1;
    }
  }
  return positions;
}

#endif
