/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file BOM.hpp
 *  @class BOM
 *  @brief Implementation of Backward Oracle Matching algorithm
 *
 * This class implements the Backward Oracle Matching algorithm.
 *
 * More information can be found in:
 *
 * ALLAUZEN C., CROCHEMORE M., RAFFINOT M., 1999,
 * Factor oracle: a new structure for pattern matching,
 * in Proceedings of SOFSEM'99, Theory and Practice of Informatics, J. Pavelka,
 * G. Tel and M. Bartosek ed., Milovy, Czech Republic, Lecture Notes in
 * Computer Science 1725, pp 291-306, Springer-Verlag, Berlin.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef BOM_HPP
#define BOM_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

#include "BOMState.hpp"

class BOM : public Algorithm {
public:
  BOM(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  using BOMStatePtr = std::shared_ptr<BOMState>;
  using Transitions = std::vector<BOMStatePtr>;

  auto preprocess() -> void;
  std::wstring pattern;

  Transitions transitions;
};

BOM::BOM(const std::wstring pattern) : pattern(pattern), transitions() {
  this->preprocess();
}

auto BOM::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  std::reverse(this->pattern.begin(), this->pattern.end());

  for (auto i = 0U; i <= m; ++i) {
    auto new_state = std::make_shared<BOMState>();
    this->transitions.emplace_back(new_state);
  }

  std::vector<signed int> S;

  S.resize(m, -1);

  for (auto i = 1U; i <= m; ++i) {
    auto label = pattern.at(i - 1);

    this->transitions.at(i - 1)->add_transition(label, i);

    signed int j = S.at(i - 1);

    auto transition_finder =
        (j != -1) ? this->transitions.at(j)->get_transition(label) : -1;

    while (j != -1 && transition_finder == -1) {
      this->transitions.at(j)->add_transition(label, i);
      j = S.at(j);
      transition_finder =
          (j != -1) ? this->transitions.at(j)->get_transition(label) : -1;
    }

    if (j == -1) {
      if (i < m) {
        S.at(i) = 0;
      }
    } else {
      if (i < m) {
        S.at(i) = this->transitions.at(j)->get_transition(label);
      }
    }
  }
}

auto BOM::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  signed int j = -1;
  size_t pos = 0;

  if (n >= m) {
    while (pos <= n - m) {
      auto current = 0;
      j = m;

      while (j > 0 and current != -1) {
        current = (current != -1)
                      ? this->transitions.at(current)->get_transition(
                            text[pos + j - 1])
                      : -1;
        --j;
      }

      if (current != -1) {
        positions.emplace_back(pos);
      }
      pos = pos + j + 1;
    }
  }
  return positions;
}

#endif
