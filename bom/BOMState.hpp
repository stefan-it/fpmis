/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file BOMState.hpp
 *  @class BOMState
 *  @brief Implementation a state for the Backward Oracle Matching algorithm
 *
 * This class implements a state used in the Backward Oracle Matching algorithm.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef BOM_STATE_HPP
#define BOM_STATE_HPP

#include <string>
#include <unordered_map>

using TransitionTable = std::map<wchar_t, size_t>;

struct BOMState {
  BOMState() : transitions() {}

  TransitionTable transitions;

  signed int get_transition(wchar_t label) {

    signed int state_id = -1;

    auto transition_finder = transitions.find(label);

    if (transition_finder != transitions.end()) {
      state_id = transition_finder->second;
    }
    return state_id;
  }

  void add_transition(wchar_t label, size_t state) {
    transitions.emplace(label, state);
  }
};

#endif
