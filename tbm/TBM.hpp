/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file TBM.hpp
 *  @class TBM
 *  @brief Implementation of Turbo-BM algorithm
 *
 * This class implements the Turbo-BM algorithm and is inspired by the C
 * implementation from Thierry Lecroq:
 *
 * http://www-igm.univ-mlv.fr/~lecroq/string/node15.html
 *
 * More information can be found in:
 *
 * CROCHEMORE, M., CZUMAJ A., GASIENIEC L., JAROMINEK S., LECROQ T.,
 * PLANDOWSKI W., RYTTER W., 1992,
 * Deux méthodes pour accélérer l'algorithme de Boyer-Moore,
 * in Théorie des Automates et Applications,
 * Actes des 2e Journées Franco-Belges, D. Krob ed., Rouen, France, 1991,
 * pp 45-63, PUR 176, Rouen, France.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef TBM_HPP
#define TBM_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class TBM : public Algorithm {
public:
  TBM(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::unordered_map<wchar_t, signed int> bad_character_table;
  std::vector<signed int> suffixes;
  std::vector<signed int> good_suffix_table;
};

TBM::TBM(const std::wstring pattern)
    : pattern(pattern), bad_character_table(), suffixes(), good_suffix_table() {
  this->preprocess();
}

auto TBM::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  auto bad_character_shift = [&]() {
    for (auto i = 0U; i < (m - 1); ++i) {
      this->bad_character_table[this->pattern[i]] = m - i - 1;
    }
  };

  auto suffixes = [&]() {
    int f, g, i;

    this->suffixes.resize(m + 1, m);

    this->suffixes[m - 1] = m;

    g = m - 1;
    f = 0;

    for (i = m - 2; i >= 0; --i) {
      if (i > g && this->suffixes[i + m - 1 - f] < i - g) {
        this->suffixes[i] = this->suffixes[i + m - 1 - f];
      } else {
        if (i < g) {
          g = i;
        }

        f = i;

        while (g >= 0 && this->pattern[g] == this->pattern[g + m - 1 - f]) {
          --g;
        }
        this->suffixes[i] = f - g;
      }
    }
  };

  auto good_suffix_shift = [&]() {
    suffixes();

    this->good_suffix_table.resize(m + 1, m);

    int i, j;

    j = 0;

    for (i = m - 1; i >= -1; --i) {
      if (i == -1 || this->suffixes[i] == i + 1) {
        for (; j < (int)m - 1 - i; ++j) {
          if (this->good_suffix_table[j] == (int)m) {
            this->good_suffix_table[j] = (int)m - 1 - i;
          }
        }
      }
    }

    for (i = 0; i <= (int)m - 2; ++i) {
      this->good_suffix_table[(int)m - 1 - this->suffixes[i]] = (int)m - 1 - i;
    }
  };

  bad_character_shift();
  good_suffix_shift();
}

auto TBM::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  int bc_shift, i, j, shift, u, v, turbo_shift;

  j = u = 0;

  shift = m;

  while (j <= (int)(n - m)) {
    i = m - 1;

    while (i >= 0 && this->pattern[i] == text[i + j]) {
      --i;

      if (u != 0 && i == (int)m - 1 - shift) {
        i -= u;
      }
    }

    if (i < 0) {
      positions.emplace_back(j);
      shift = this->good_suffix_table[0];
      u = m - shift;
    } else {
      v = m - 1 - i;

      turbo_shift = u - v;

      auto char_finder = this->bad_character_table.find(text[i + j]);

      bc_shift =
          (char_finder != this->bad_character_table.end() ? char_finder->second
                                                          : (int)m) -
          (int)m + 1 + i;

      shift = std::max(turbo_shift, bc_shift);
      shift = std::max(shift, this->good_suffix_table[i]);

      if (shift == this->good_suffix_table[i]) {
        u = std::min((int)m - shift, v);
      } else {
        if (turbo_shift < bc_shift) {
          shift = std::max(shift, u + 1);
        }
        u = 0;
      }
    }

    j += shift;
  }

  return positions;
}

#endif
