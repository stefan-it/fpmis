#include <boost/locale.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include "algorithm/StableAlgorithmFactory.hpp"
#include "bndm/BNDM.hpp"

namespace po = boost::program_options;

static po::options_description DESC("Options");
static po::variables_map VARS;

static auto setup_description() -> void {
  DESC.add_options()("filename,f", po::value<std::string>(), "Input file")(
      "algorithm,a", po::wvalue<std::wstring>(),
      "algorithms: bm, bndm, bom, bsdm, horspool, kmpfull, kmpnaive, qs, "
      "shiftand, tbm")("pattern,p", po::value<std::string>(), "Search pattern")(
      "count,c", po::value<bool>()->default_value(false),
      "Counts number of occurences only (instead of print positional "
      "information)")("help,h", "Displays help page");
}

static auto parse_program_options(int argc, char **argv) -> void {
  po::store(po::parse_command_line(argc, argv, DESC), VARS);
  po::notify(VARS);
}

auto main(int argc, char **argv) -> int {
  std::setlocale(LC_ALL, "");

  setup_description();
  parse_program_options(argc, argv);

  if (VARS.count("help")) {
    std::cerr << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  if (!VARS.count("filename")) {
    std::cerr << "Please specify a filename!\n";
    std::cerr << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  if (!VARS.count("pattern")) {
    std::cerr << "Please specify a search pattern!\n";
    std::cerr << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  if (!VARS.count("algorithm")) {
    std::cerr << "Please specify a search algorithm!\n";
    std::cerr << DESC << std::endl;
    return EXIT_SUCCESS;
  }

  std::wifstream input_fs(VARS["filename"].as<std::string>());
  input_fs.imbue(std::locale(""));

  std::wstring current_line;

  std::vector<std::pair<size_t, std::vector<size_t>>> positional_occurences;

  auto algorithm_factory = std::make_unique<StableAlgorithmFactory>();

  auto algorithm =
      algorithm_factory->get_algorithm(VARS["algorithm"].as<std::wstring>(),
                                       boost::locale::conv::utf_to_utf<wchar_t>(
                                           VARS["pattern"].as<std::string>()));

  size_t line_counter = 1;
  size_t occurence_counter = 0;

  double search_duration = 0;

  while (std::getline(input_fs, current_line)) {
    auto current_search_time = std::clock();
    auto current_positions = algorithm->search(current_line);
    search_duration +=
        (std::clock() - current_search_time) / (double)CLOCKS_PER_SEC;

    if (VARS["count"].as<bool>()) {
      occurence_counter += current_positions.size();
    } else {
      if (current_positions.size() > 0) {
        positional_occurences.emplace_back(line_counter, current_positions);
      }
    }
    ++line_counter;
  }

  if (VARS["count"].as<bool>()) {
    std::wcerr << "Number of occurences found: " << occurence_counter << "\n";
  } else {
    for (auto &line : positional_occurences) {
      std::wcerr << "Positions found in line: " << line.first << ": ";

      for (auto &position : line.second) {
        std::wcerr << position << ", ";
      }
      std::wcerr << "\n";
    }
  }

  std::wcerr << "Searching time (seconds): " << search_duration << "\n";

  return 0;
}
