# "Flexible Pattern Matching in Strings" (*FPMIS*)

This is a C++14 header-only implementation of some algorithms for flexible
pattern matching in strings.

## References

* "Flexible Pattern Matching in Strings" by Gonzalo Navarro and Mathieu Raffinot
  , see it [here](https://www.dcc.uchile.cl/~gnavarro/FPMbook/).

* "Handbook of Exact String Matching Algorithms" by Christian Charras and
  Thierry Lecroq, see it [here](http://www-igm.univ-mlv.fr/~lecroq/string/).

* "SMART: A String Matching Algorithms Research Tool" by Simone Faro and
  Thierry Lecroq, see it [here](http://www.dmi.unict.it/~faro/smart/).

* This implementation was done during the computational linguistics
  M.Sc. course (winter term)
  ["Flexible Patternsuche auf Strings"](http://www.cis.uni-muenchen.de/people/Schulz/teaching.html) held by
  [Prof. Dr. Klaus U. Schulz](http://www.cis.uni-muenchen.de/people/schulz.html)
  at [The Center for Information and Language Processing (CIS)](http://www.cis.lmu.de/index.html), LMU Munich.

## Algorithms

The following algorithms are implemented:

* *Boyer-Moore*
* *Backward Nondeterministic Dawg Matching*
* *Backward Oracle Matching*
* *Backward SNR DAWG Matching*
* *Horspool*
* *Knuth-Morris-Pratt* (full and naive)
* *Quick Search*
* *Shift And*
* *Turbo-BM*

# Build status

[![build status](https://gitlab.com/stefan-it/fpmis/badges/master/build.svg)](https://gitlab.com/stefan-it/fpmis/commits/master)

# Compiling the example program

*FPMIS* will currently work on *Unix* and *Linux* based systems. There is no plan
to support Window$ in near future. Following dependencies must be installed to
build *FPMIS*:

* *GNU* `make`
* *GCC* >= 5.3 - as we heavily use new *C++11* and *C++14* features
* `clang` is fully supported - you are welcome to build *FPMIS* with that compiler
* `Boost` >= 1.44 - *FPMIS* needs: `program_options`, `locale` and `unit_test_framework`

After all dependencies have been installed, you can compile *FPMIS* with the
following commands:

```bash
$ mkdir build && cd $_
$ cmake ..
$ make
```

To execute the example program:

```bash
$ ./bin/fpmis_example --help
Options:
  -f [ --filename ] arg   Input file
  -a [ --algorithm ] arg  algorithms: bm, bndm, bom, bsdm, horspool, kmpfull,
                          kmpnaive, qs, shiftand, tbm
  -p [ --pattern ] arg    Search pattern
  -c [ --count ] arg (=0) Counts number of occurences only (instead of print
                          positional information)
  -h [ --help ]           Displays help page
```

# Benchmark

The following benchmark shows the search time for the patterns "ab", "einen"
and "übergangsweise" on the German [Europarl Corpus](http://www.statmt.org/europarl/).
The corpus itself has a size of 314 MB and 1920209 lines. The benchmark was done
on an Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz. Median of three runs for each
algorithm is used as final result.

## Pattern "ab"

| Algorithm | Search time (seconds)
| --------- | ---------------------
| bm        |  4.5353
| bndm      | 37.6433
| bom       |  2.5084
| bsdm      |  2.8021
| horspool  |  4.2593
| kmpfull   |  2.3732
| kmpnaive  |  **2.3659**
| qs        |  3.8329
| shiftand  | 82.4339
| tbm       |  4.5483

## Pattern "einen"

| Algorithm | Search time (seconds)
| --------- | ---------------------
| bm        |  3.3675
| bndm      | 21.2611
| bom       |  2.6210
| bsdm      |  **2.2919**
| horspool  |  3.3199
| kmpfull   |  2.7150
| kmpnaive  |  2.7357
| qs        |  3.3851
| shiftand  | 83.8682
| tbm       |  3.3278

## Pattern "übergangsweise"

| Algorithm | Search time (seconds)
| --------- | ---------------------
| bm        |  2.2646
| bndm      |  9.0879
| bom       |  1.9042
| bsdm      |  **1.8149**
| horspool  |  2.2787
| kmpfull   |  2.1649
| kmpnaive  |  2.1711
| qs        |  2.2596
| shiftand  | 85.0816
| tbm       |  2.2899

# Contact (Bugs, Feedback, Contribution and more)

For questions about *FPMIS*, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
this implementation is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.

# Acknowledgements

I want to thank [Prof. Dr. Klaus U. Schulz](http://www.cis.uni-muenchen.de/people/schulz.html)
for his patience during the winter term course. His great explanations
motivated and helped to deeply understand various string matching algorithms!
