/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file BSDM.hpp
 *  @class BSDM
 *  @brief Implementation of Backward SNR DAWG Matching algorithm
 *
 * This class implements the Backward SNR DAWG Matching algorithm inspired
 * by this C implementation:
 *
 * https://github.com/smart-tool/smart/blob/master/source/algos/bsdm2.c
 *
 * More information can be found in:
 *
 * FARO S., LECROQ T., 2012
 * A Fast Suffix Automata Based Algorithm for Exact Online String Matching.
 * 17-th International Conference on Implementation and Application of Automata
 * - CIAA , pp.146--160 (2012).
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 * @note We use a value of 2 for both q and b as described in the original
 * paper.
 */

#ifndef BSDM_HPP
#define BSDM_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

#define HS(x, i) (x[i] << 2) + x[i + 1]
#define Q 2

class BSDM : public Algorithm {
public:
  BSDM(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::map<signed int, size_t> pos;

  size_t s;
  size_t len;
};

BSDM::BSDM(const std::wstring pattern) : pattern(pattern), pos(), s(0), len(0) {
  this->preprocess();
}

auto BSDM::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  size_t i = 0;
  size_t j = 0;

  std::vector<wchar_t> occ;

  for (; i < m - Q + 1; ++i) {
    auto c = HS(this->pattern, i);
    auto found = std::find(occ.begin(), occ.end(), c) != occ.end();

    if (found) {
      auto dch = HS(this->pattern, j);
      while (dch != c) {
        ++j;
        dch = HS(this->pattern, j);
      }
      ++j;
    }

    occ.emplace_back(c);

    if (this->len < i - j + 1) {
      this->len = i - j + 1;
      this->s = j;
    }
  }

  occ.clear();

  for (i = 0U; i < this->len; ++i) {
    auto c = HS(this->pattern, this->s + i);
    pos.emplace(c, i);
  }
}

auto BSDM::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  size_t r = this->len + this->s - 1;
  size_t j = this->len - 1;
  size_t k = 0;

  if (n >= m) {
    while (j < n) {
      auto c = HS(text, j);
      auto finder = this->pos.find(c);

      if (finder != this->pos.end()) {
        k = 1;

        auto i = finder->second;

        while (k <= i && this->pattern.at(this->s + i - k) == text.at(j - k)) {
          ++k;
        }

        if (k > i) {
          if (k == this->len) {
            if (!wmemcmp(this->pattern.c_str(), text.c_str() + j - r, m) &&
                j - r <= n - m) {
              positions.emplace_back(j - this->s - this->len + 1);
            }
          } else {
            j -= k;
          }
        }
      }
      j += this->len;
    }
  }

  return positions;
}

#endif
