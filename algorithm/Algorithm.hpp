/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file Algorithm.hpp
 *  @class Algorithm
 *  @brief Base class for an algorithm
 *
 * This class implements a base class for an algorithm.
 *
 * The concrete class should override and implement the search() method.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

#include <string>
#include <vector>

class Algorithm {
public:
  virtual ~Algorithm() noexcept = default;

  virtual auto search(const std::wstring text) -> std::vector<size_t> const = 0;
};

#endif
