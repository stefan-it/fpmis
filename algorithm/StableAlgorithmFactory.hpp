/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file StableAlgorithmFactory.hpp
 *  @class StableAlgorithmFactory
 *  @brief Concrete algorithm factory implementation
 *
 * This class implements all concrete methods of the algorithm factory.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef STABLE_ALGORITHM_FACTORY_HPP
#define STABLE_ALGORITHM_FACTORY_HPP

#include <memory>
#include <string>

#include "../bm/BM.hpp"
#include "../bndm/BNDM.hpp"
#include "../bom/BOM.hpp"
#include "../bsdm/BSDM.hpp"
#include "../horspool/Horspool.hpp"
#include "../kmp/KMPFull.hpp"
#include "../kmp/KMPNaive.hpp"
#include "../qs/QS.hpp"
#include "../shift_and/ShiftAnd.hpp"
#include "../tbm/TBM.hpp"
#include "AlgorithmFactory.hpp"

class StableAlgorithmFactory : public AlgorithmFactory {
public:
  virtual auto get_algorithm(const std::wstring algorithm_name,
                             const std::wstring pattern)
      -> algorithm_ptr override {
    if (algorithm_name == L"bndm") {
      return std::make_unique<BNDM>(pattern);
    } else if (algorithm_name == L"horspool") {
      return std::make_unique<Horspool>(pattern);
    } else if (algorithm_name == L"shiftand") {
      return std::make_unique<ShiftAnd>(pattern);
    } else if (algorithm_name == L"bom") {
      return std::make_unique<BOM>(pattern);
    } else if (algorithm_name == L"kmpfull") {
      return std::make_unique<KMPFull>(pattern);
    } else if (algorithm_name == L"kmpnaive") {
      return std::make_unique<KMPNaive>(pattern);
    } else if (algorithm_name == L"bm") {
      return std::make_unique<BM>(pattern);
    } else if (algorithm_name == L"tbm") {
      return std::make_unique<TBM>(pattern);
    } else if (algorithm_name == L"qs") {
      return std::make_unique<QS>(pattern);
    } else if (algorithm_name == L"bsdm") {
      return std::make_unique<BSDM>(pattern);
    } else {
      return nullptr;
    }
  }
};

#endif
