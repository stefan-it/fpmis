/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file AlgorithmFactory.hpp
 *  @class AlgorithmFactory
 *  @brief Base class for an algorithm factory
 *
 * This class implements a base class for an algorithm factory.
 *
 * The concrete class should override and implement the get_algorithm() method.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef ALGORITHM_FACTORY_HPP
#define ALGORITHM_FACTORY_HPP

#include <memory>
#include <string>

#include "Algorithm.hpp"

struct AlgorithmFactory {
  using algorithm_ptr = std::unique_ptr<Algorithm>;
  virtual ~AlgorithmFactory() noexcept = default;

  virtual auto get_algorithm(const std::wstring algorithm_name,
                             const std::wstring pattern) -> algorithm_ptr = 0;
};

#endif
