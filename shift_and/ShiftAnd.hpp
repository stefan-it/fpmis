/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file ShiftAnd.hpp
 *  @class ShiftAnd
 *  @brief Implementation of Shift And algorithm
 *
 * This class implements the Shift And algorithm.
 *
 * More information can be found in:
 *
 * NAVARRO G., RAFFINOT M., 2002,
 * Flexible Pattern Matching in Strings:
 * Practical On-line Search Algorithms for Texts and Biological Sequences,
 * Cambridge University Press
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef SHIFT_AND_HPP
#define SHIFT_AND_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class ShiftAnd : public Algorithm {
public:
  ShiftAnd(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::map<wchar_t, boost::dynamic_bitset<>> table;
};

ShiftAnd::ShiftAnd(const std::wstring pattern) : pattern(pattern), table() {
  this->preprocess();
}

auto ShiftAnd::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());

  boost::dynamic_bitset<> empty_bitset(m);

  for (auto i = 0U; i < m; ++i) {
    auto char_finder = this->table.find(pattern[i]);

    if (char_finder == this->table.end()) {
      auto new_bitset = empty_bitset;
      new_bitset[i] = true;

      this->table.emplace(pattern[i], new_bitset);
    } else {
      char_finder->second[i] = true;
    }
  }
}

auto ShiftAnd::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  boost::dynamic_bitset<> D(m);

  auto zeros_bitset = boost::dynamic_bitset<>{m};
  auto ones_bitset = boost::dynamic_bitset<>{m, 1};

  if (n >= m) {
    for (auto pos = 0U; pos < n; ++pos) {
      auto char_finder = this->table.find(text[pos]);
      auto bit_vector =
          char_finder != this->table.end() ? char_finder->second : zeros_bitset;

      D = ((D << 1) | ones_bitset) & bit_vector;

      if (D[m - 1]) {
        positions.emplace_back(pos - m + 1);
      }
    }
  }
  return positions;
}

#endif
