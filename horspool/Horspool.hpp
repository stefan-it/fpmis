/*
 * This file is part of fpmis.
 *
 * fpmis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file Horspool.hpp
 *  @class Horspool
 *  @brief Implementation of Horspool algorithm
 *
 * This class implements the Horspool algorithm.
 *
 * More information can be found in:
 *
 * HORSPOOL R.N., 1980, Practical fast searching in strings,
 * Software - Practice & Experience, 10(6):501-506.
 *
 * @author Stefan Schweter <stefan@schweter.it> (2017)
 */

#ifndef HORSPOOL_HPP
#define HORSPOOL_HPP

#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <unordered_map>
#include <vector>

#include "../algorithm/Algorithm.hpp"

class Horspool : public Algorithm {
public:
  Horspool(const std::wstring pattern);

  auto search(const std::wstring text) -> std::vector<size_t> const override;

private:
  auto preprocess() -> void;
  std::wstring pattern;

  std::unordered_map<wchar_t, size_t> table;
};

Horspool::Horspool(const std::wstring pattern) : pattern(pattern), table() {
  this->preprocess();
}

auto Horspool::preprocess() -> void {
  const size_t m = static_cast<size_t>(this->pattern.size());
  for (auto i = 0U; i < (m - 1); ++i) {
    this->table[this->pattern[i]] = m - i - 1;
  }
}

auto Horspool::search(const std::wstring text) -> std::vector<size_t> const {
  const size_t m = static_cast<size_t>(this->pattern.size());
  const size_t n = static_cast<size_t>(text.size());

  std::vector<size_t> positions;

  if (n >= m) {
    for (auto pos = 0U; pos <= (n - m);) {
      auto j = m;

      for (; j and text[pos + j - 1] == this->pattern[j - 1]; --j) {
      };

      if (j == 0) {
        positions.emplace_back(pos);
      }
      auto char_finder = table.find(text[pos + m - 1]);

      pos += char_finder != table.end() ? char_finder->second : m;
    }
  }
  return positions;
}

#endif
