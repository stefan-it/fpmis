enable_testing()

set (
  TEST_CASES
)

foreach(TEST_CASE ${TEST_CASES})
  add_test (
    NAME ${TEST_CASE}
    COMMAND ${EXECUTABLE_OUTPUT_PATH}/${TEST_CASE}
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  )
endforeach(TEST_CASE)
